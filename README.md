# Git

## Git clone

Input
```bash
git clone https://gitlab.com/p8456/git.git
```

Output
```bash
Cloning into 'git'...
Username for 'https://gitlab.com': anaframework
Password for 'https://anaframework@gitlab.com': 
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 2.77 KiB | 2.77 MiB/s, done.
```

## Commit

Input
```bash
git status
```

Output
```bash
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	test.py

nothing added to commit but untracked files present (use "git add" to track)
```

Input
```bash
git add .
```

Input
```bash
git status
```

Output
```bash
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   test.py
```

Input
```bash
git commit -m 'Firs commit'
```

Output
```bash
[main dc8ff58] Firs commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test.py
```
